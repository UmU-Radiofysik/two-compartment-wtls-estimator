function [x,f] = newton_wtls_estimator(t,Cp,Ct,nIter,relTolX,I,Q,Q2,dW,QQPerm)
    % WTLS estimator for both the 2CXM and the 2CFM 
    %
    % Input:
    % t         - Time vector
    % Cp        - AIF
    % Ct        - Tissue CA conc. curve
    % nIter     - Max # of iterations
    % relTolX   - Termination criteria 
    % I         - Identity matrix n x n
    % Q         - Quadrature operator n x n
    % Q2        - Quadrature operator squared n x n
    % dW        - Derivative of the W matrix
    % QQPer     - Cell array of polynomials of the Q {Q2*Q2',Q2*Q',Q2 + Q2',Q*Q2',Q*Q',Q + Q'}
    %
    % Output:
    % x         - Estimated functions of p. I.e. x is the result of the solved
    %             linear system of equations
    % f         - The objective function at each iteration.
    
 
    % Cholesky solver settings
    opts1.UT = true; opts1.TRANSA = true;
    opts2.UT = true; opts2.TRANSA = false;
    
    % Rearrange vectors to be consistent
    t = t(:);
    Cp = Cp(:);
    Ct = Ct(:);


    % Get primitive function
    CT = cumtrapz(t,Ct);
    CP = cumtrapz(t,Cp);
    CTT = cumtrapz(t,CT);
    CPP = cumtrapz(t,CP);

    % Solve the overdetermined system Ax = B to get initial guess
    A = [-CTT, -CT, CPP CP];
    x = A\Ct;

    
    % Allocation
    terminate = false;
    g = zeros(4,1);
    h = zeros(4,4);
    dWTq = cell(1,4);
    dq = cell(1,4);
    iteration = 0;
    W = zeros(size(Q,1),size(Q,2)*2);

    
    while (~terminate)
        W(:,1:size(Q,2)) = -x(1)*Q2 - x(2)*Q - I;
        W(:,(size(Q,2)+1):end) =  x(3)*Q2 + x(4)*Q;
        
        % Perform S = W*W' in a faster manner
        S = (x(1)^2+x(3)^2)*QQPerm{1} + (x(1)*x(2)+x(3)*x(4))*QQPerm{2} + x(1)*QQPerm{3} + (x(1)*x(2)+x(3)*x(4))*QQPerm{4} + (x(2)^2 + x(4)^2)*QQPerm{5} + x(2)*QQPerm{6} + I;
        R = chol(S);
        z = A*x - Ct;
        % Solve q = S\z using the Cholesky decomp;
        ee = linsolve(R,z,opts1);
        q = linsolve(R,ee,opts2);

        
        % Objective function
        iteration = iteration + 1;
        f(iteration) = z'*q; %#ok<AGROW>
        
        
        % The gradient g
        aTq = A'*q;
        WTq = W'*q;
        for ii = 1:4
            dWTq{ii} = dW{ii}'*q;
            g(ii) = 2 * (aTq(ii) - (dWTq{ii}'*WTq));
            % Solve S*dq{ii} = e0 using cholesky decomp.
            e0 = (-(dW{ii}*WTq) - (W*dWTq{ii}) + A(:,ii));
            ee = linsolve(R,e0,opts1);
            dq{ii} = linsolve(R,ee,opts2);
        end

        
        % Hessian
        for ii = 1:4
            AiiT = A(:,ii)';
            for jj = 1:ii
                dWTdq = dW{ii}'*dq{jj};
                WTdq = W'*dq{jj};
                h(ii,jj) = 2 * (AiiT*dq{jj} - dWTdq'*WTq - dWTq{ii}'*WTdq - dWTq{ii}'*dWTq{jj});
                h(jj,ii) = h(ii,jj);
            end
        end
        

        % Newton step
        x_old = x;
        x = x - h\g;

        % Terminate?
        err = norm(x-x_old)/norm(x);
        if (err < relTolX || iteration >= nIter)
          terminate = true;  
          continue;
        end
        
    end
    

end