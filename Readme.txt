Use this script to produce the result in the paper: "Parameter Estimation 
using Weighted Total Least Squares in the Two-Compartment Exchange Model" 

To produce the results (takes about 1h per model) use the file main.m. This script
performs:
- Error calculation for the 2CXM or 2CFM using LLS, NLLS and WTLS.
- Timing analysis for the different estimators
- Creating plots.

Written by Anders Garpebring and Tommy L�fstedt 2016-2017 (c). You may use/modify and
redistribute the code. If this code is used in other work please cite the publication.

NOTE: The code has be executed using MATLAB 2016a and 2016b. Likeliy it works with other 
versions too. But that has not been confirmed by the autors.