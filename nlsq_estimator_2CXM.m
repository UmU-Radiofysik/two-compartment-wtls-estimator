% Non-linear least squares fit to the 2CXM 
function p = nlsq_estimator_2CXM(t,Cp,Ct,settings)
% Perform a NLLS fit to the 2CXM.
% Input:
% t         - time vector [min]
% Cp        - AIF [mM]
% Ct        - Tissue CA curve [mM]
% settings  - Options for the lsqcurvefit algorithm
%
% Output:
% p         - Estimated parameters [Fp, PS,vp, ve]


% Inital guess
p0 = llsq_estimator_2CXM(t,Cp,Ct);

p = lsqcurvefit(@(p,t)(model2CXM(t,Cp,p)),p0,t,Ct,[],[],settings);
p = p(:);
end




