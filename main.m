%% Description
% Run this script to produce the result in the paper: "Parameter Estimation 
% using Weighted Total Least Squares in the Two-Compartment Exchange Model".
%
% Before you can run the script you should tell where the
% results should be stored. You do this by setting the 'resultPath'
% variable to desired location. If you don't the results will be stored in 
% the current folder.  
%
% The simulation requires about 1h to complete for one of the models 2CXM
% and 2CFM. To run faster, use fewer simulated samples and fewer samples in
% the timing analysis. To do this set 'nSim' and 'nSimTiming' to smaller values.
% (This will result in more noisy results.)
%
% Tasks performed:
% - Error calculation for the 2CXM and 2CFM using LLS, NLLS and WTLS.
% - Timing analysis for the different estimators
% - Creating plots.
%
% NOTE: A single run only produces results for one model (2CXM or 2CFM).
% Change model to get result for the other model too.
%
% Written by Anders Garpebring and Tommy L�fstedt 2016-2017 (c). You may use/modify and
% redistribute the code. If this code is used in other work please cite the publication.

%% Prepatation
clear all; %#ok<CLALL>
close all hidden

% To get identical results. Change if desired.
seed = 101230;   


%% Simulation settings

% Where to store the results. Should be provided if you want your results 
% somewhere else than in the current location.
resultPath = 'F:\Optimal noise in DCE-MRI PK fitting';

% The (partial) name of the results stored. NOTE: This is only part of the 
% name of the file. The filename also contains the model name and the number 
% of samples. If an idential simulation (same name, model and # of samples)
% is rerun (result file already exists). The analysis will NOT be done again,
% instead the file will be loaded and plots will be produced.
fileName = 'results';

% What model to use (2CFM or 2CXM)
model = '2CXM';

% Sampling times simulated.
Dt = [1.25 3 5]/60; % [min]

% Total simulation time
T = 5; % [min]

% SNRs explored. SNR defined as max(Ct)/sigma
SNR = [5:10,12:2:20,25:5:40];

% Number of (MC) samples simulated. Modify this for quick results.
nSim = 10000;

% Number of (MC) sampels used when timing the estimators. Modify this for quick results.
nSimTiming = 1000;

% Samples simulated when checking the time required. (The number of timepoints 
% in the DCE-MRI CA curve.) 
NSamplesTimingSimulation = [60:79,80:4:300]; 

% Sampling time used when generating the CA conc. curves (Ct).
dt_ref = 0.1/60; % [min];

% Blood Hematocrit
hct = 0.43;

% Simulated ranges of parameters for the 2CFM and the 2CXM. Note the order
% of the parameters: [Fp PS vp ve]
p_limits_2CFM = [1.07 ,2.22; 0.05, 0.30; 0.17, 0.31; 0.18, 0.62]; 
p_limits_2CXM = [0.139*(1-hct) ,0.586*(1-hct);  0.003, 0.141; 0.096*(1-hct), 0.198*(1-hct); 0.06, 0.428];

% Optimizer settings for the NLLS estimator
settings = optimset('lsqcurvefit');
settings.Display = 'off';
settings.MaxFunEvals = 1000;
settings.Algorithm = 'levenberg-marquardt';
settings.MaxIter = 1000;
settings.TolX = 1e-3;
settings.TolFun = 1e-3;

% Optimizer settings for the ML-LLS
settingsWTLS.nIter = 2;
settingsWTLS.relTolX = 1e-3;


resultFile = fullfile(resultPath,[fileName,'_',model,'_',num2str(nSim),'.mat']);
tref = 0:dt_ref:T;

% Selected method based on what model is analysed
switch (model)
    case '2CFM'
        Model           = @model2CFM;
        NLLS_estimator  = @nlsq_estimator_2CFM;
        LLS_estimator   = @llsq_estimator_2CFM;
        WTLS_estimator = @newton_wtls_estimator_2CFM;
        p_limits = p_limits_2CFM;
        
    case '2CXM'
        Model = @model2CXM;
        NLLS_estimator  = @nlsq_estimator_2CXM;
        LLS_estimator   = @llsq_estimator_2CXM;
        WTLS_estimator = @newton_wtls_estimator_2CXM;
        
        p_limits = p_limits_2CXM;
end

clc;
disp('-------------------------------------------------------------------')
disp(['Simulate the performance of ',model, ' using LLS, NLS and WTLS.'])

% Note! The simulation will not be run if the data already is available. To
% rerun a simulation change the name of outputfile in the fileName variable
% above.
if (~exist(resultFile,'file'))

    
%% Perform simulations by looping over sampling times and SNR

    % Allocate space
    p_nlls  = zeros(4, numel(Dt), numel(SNR), nSim);
    p_lls   = zeros(4, numel(Dt), numel(SNR), nSim);
    p_wtls = zeros(4, numel(Dt), numel(SNR), nSim);
    p_true  = zeros(4, numel(Dt), numel(SNR), nSim);
    p_diff  = zeros(4, numel(Dt), numel(SNR), nSim);

    
    Ntot = numel(Dt)*numel(SNR)*nSim;
    iN = 0;
    
    disp(' ')
    disp('Start simulating accuracy and precision')
    
    progress = Progressor('Simulation progress');
    
    for iDt = 1:numel(Dt)
        
        dt = Dt(iDt);
        t = 0:dt:T;
        
        [I,Q,Q2,dW,QQperm] = getWTLSEstimatorPrecomputation(t);
        
        disp(['Simulating timestep ',num2str(iDt), ' of ',num2str(numel(Dt)),': Dt = ',num2str(dt*60,3),'s.'])
        
        
        for iSNR = 1:numel(SNR)
            snr = SNR(iSNR);
            
            
            
            
            for iSim = 1:nSim
                
                % Get random parameter
                p = randomParameters(p_limits);
                
                % Get reference curves. Add a random jitter to ensure that
                % the sampling delay does not introduce a bias.
                delay = 20/60 + dt*(rand(1)-0.5);
                Cp_ref =  parkerAIF(tref,delay);
                Ct_ref = Model(tref,Cp_ref,p);
                
                % Find out the noise level
                noiseLevel = max(Ct_ref)/snr;
                
                % Get sampled curves with noise
                Ct = interp1(tref,Ct_ref,t) + noiseLevel*randn(size(t));
                Cp = interp1(tref,Cp_ref,t) + noiseLevel*randn(size(t));
                
                % Save true value
                p_true(:,iDt,iSNR,iSim) = p;
                
                % Estimate parameters
                % NLLS
                p_nlls(:,iDt,iSNR,iSim) = NLLS_estimator(t,Cp,Ct,settings);
                % LLS
                [p_lls(:,iDt,iSNR,iSim),x0] = LLS_estimator(t,Cp,Ct);
                % ML-LLS
                p_wtls(:,iDt,iSNR,iSim) = WTLS_estimator(t,Cp,Ct,settingsWTLS.nIter,settingsWTLS.relTolX,I,Q,Q2,dW,QQperm);
                
                iN = iN + 1;
            end
            
            % Show progress
            progress.setProgress(iN/Ntot);
        end
    end
    progress.setProgress(1);
    delete(progress);
    
    disp('Done simulating accuracy and precision')
    disp(' ')
    
%%  Estimate the required simulation time
    disp('Simulating the required time for the LLS, NLS and WTLS metods.');
    
    
    
    iN = 0;
    DtTime = T./NSamplesTimingSimulation;
    
    t_nlls = zeros(numel(DtTime),nSimTiming);
    t_lls = zeros(numel(DtTime),  nSimTiming);
    t_wtls = zeros(numel(DtTime),  nSimTiming);
    Ntot = numel(DtTime)*nSimTiming;
    
    progress = Progressor('Simulation progress');

    % Loop over sampling times
    for iDt = 1:numel(DtTime)
        
        dt = DtTime(iDt);
        t = 0:dt:T;
        
        [I,Q,Q2,dW,QQperm] = getWTLSEstimatorPrecomputation(t);
        Cp0 = parkerAIF(t,20/60);
        
        snr = 20;
        
        
        for iSim = 1:nSimTiming
            % Get random parameter
            p = randomParameters(p_limits);
            
            % Get reference curve
            Ct_ref = Model(tref,Cp_ref,p);
            
            % Find out the noise level
            noiseLevel = max(Ct_ref)/snr;
            
            % Get sampled curves with noise
            Ct = interp1(tref,Ct_ref,t) + noiseLevel*randn(size(t));
            Cp = Cp0 + noiseLevel*randn(size(t));
            
            % Estimate parameters
            
            % NLLS
            tic;
            p1 = NLLS_estimator(t,Cp,Ct,settings);
            t_nlls(iDt,iSim) = toc;
            
            % LLS
            tic;
            p2 = LLS_estimator(t,Cp,Ct);
            t_lls(iDt,iSim) = toc;
            
            % WTLS
            tic;
            p3 = WTLS_estimator(t,Cp,Ct,settingsWTLS.nIter,settingsWTLS.relTolX,I,Q,Q2,dW, QQperm);
            t_wtls(iDt,iSim) = toc;
            
            
            % Show progress
            iN = iN + 1;  
        end
        % Show progress
        progress.setProgress(iN/Ntot);
    end
    
    progress.setProgress(1);
    delete(progress);
    
    disp('Done simulating the required time.')

    % Save the data
    disp(['Save results in: ',resultFile])
    save(resultFile, 'p_nlls', 'p_lls', 'p_wtls', 'p_true', 'p_diff', 't_nlls', 't_lls', 't_wtls', 'model', 'Dt', 'DtTime', 'T', 'SNR', 'nSim', 'dt_ref', 'tref', 'p_limits', 'settings', 'settingsWTLS', 'NSamplesTimingSimulation');
end




%% Post processing - Plots for the Paper
disp(' ')
disp(['Load results from: ', resultFile])
data = load(resultFile);

disp('Produce plots')

% Font sizes
titleFontSize = 14;
labelFontSize = 13;

prcMin = 5;
prcMax = 95;

p_nlls = data.p_nlls;
p_true = data.p_true;
e = (p_nlls - p_true)./p_true;
accuracy_nlls = median(e,4);
precision_nlls = prctile(e,prcMax,4)-prctile(e,prcMin,4);

p_lls = data.p_lls;
e = (p_lls - p_true)./p_true;
accuracy_lls = median(e,4);
precision_lls = prctile(e,prcMax,4)-prctile(e,prcMin,4);

p_wtls = data.p_wtls;
e = (p_wtls - p_true)./p_true;
accuracy_wtls = median(e,4);
precision_wtls = prctile(e,prcMax,4)-prctile(e,prcMin,4);



%% Accuracy plot
H = figure(1);
H.Position = [30,30,1000,800];
clf
lw = 1.5;
col = {[1 0 0]/1.2,[0 1 0]/1.5,[0 0 1]/1.3};

if isequal('2CXM',model)
    ylimAccuracy = {[-20, 2],[-30, 2.5],[-30, 5];...
        [-30 2.5],[-40 5],[-60 5];...
        [-2 20],[-3 40],[-4 40];...
        [-1 1],[-20 2.5],[-50 5]};
else
    ylimAccuracy = {[-15, 2],[-20, 5],[-25, 10];...
        [-30 2.5],[-40 5],[-60 5];...
        [-2 20],[-3 40],[-4 40];...
        [-2 1.5],[-20 2.5],[-50 5]};
end
delta = [0.051,0.051];

pLabel = {'Accuracy $F_p$ [\%]','Accuracy $PS$ [\%]','Accuracy $v_p$ [\%]','Accuracy $v_e$ [\%]'};
for it = 1:numel(data.Dt)
    for iP = 1:4
       h = modsubplot(4,3,(iP-1)*3 + it, delta);
       plot(data.SNR,100*squeeze(accuracy_nlls(iP,it,:)),'-','LineWidth',lw,'color',col{1})
       hold on
       plot(data.SNR,100*squeeze(accuracy_lls(iP,it,:)),'-','LineWidth',lw,'color',col{2})
       plot(data.SNR,100*squeeze(accuracy_wtls(iP,it,:)),'-','LineWidth',lw,'color',col{3})

       hold off
       if (it == 1 && iP == 1)
           l = legend('NLLS','LLS','WTLS');
           l.Interpreter = 'Latex';
       end
       if (iP == 1)
           title(['$\Delta t = ',num2str(60*data.Dt(it)),'$ s'],'Interpreter','latex','FontSize',titleFontSize)
       end
       if (iP == 4)
           xlabel('SNR','Interpreter','latex','FontSize',labelFontSize)
       end
       
       if (it == 1)
           ylabel(pLabel{iP},'Interpreter','latex','FontSize',labelFontSize);
       end
       
       grid on
       xlim([5 40])
       
       if (iP ~= 4)
          h.XTickLabel = {}; 
       end
       
       ylim(ylimAccuracy{iP,it});
    end
end

%% Precision plot
H = figure(2);
H.Position = [30,30,1000,800];
clf
lw = 1.5;
col = {[1 0 0]/1.2,[0 1 0]/1.5,[0 0 1]/1.3};
if isequal('2CXM',model)
    ylimPrecision = {[0, 60],[0, 150],[0, 200];...
        [0 150],[0 200],[0 200];...
        [0 150],[0 250],[0 300];...
        [0 400],[0 600],[0 1000]};
else
    ylimPrecision = {[0, 60],[0, 150],[0, 200];...
        [0 150],[0 200],[0 200];...
        [0 100],[0 250],[0 300];...
        [0 400],[0 600],[0 1000]};
end

delta = [0.051,0.051];
pLabel = {'Precision $F_p$ [\%]','Precision $PS$ [\%]','Precision $v_p$ [\%]','Precision $v_e$ [\%]'};
for it = 1:numel(data.Dt)
    for iP = 1:4

       h = modsubplot(4,3,(iP-1)*3 + it,delta);
       plot(data.SNR,100*squeeze(precision_nlls(iP,it,:)),'-','LineWidth',lw,'color',col{1})
       hold on
       plot(data.SNR,100*squeeze(precision_lls(iP,it,:)),'-','LineWidth',lw,'color',col{2})
       plot(data.SNR,100*squeeze(precision_wtls(iP,it,:)),'-','LineWidth',lw,'color',col{3})

       hold off
       if (it == 1 && iP == 1)
          l = legend('NLLS','LLS','WTLS');
          l.Interpreter = 'Latex';
       end
       if (iP == 1)
          title(['$\Delta t = ',num2str(60*data.Dt(it)),'$ s'],'Interpreter','latex','FontSize',titleFontSize) 
       end
       if (iP == 4)
          xlabel('SNR','Interpreter','latex','FontSize',labelFontSize) 
       end
       
       if (it == 1)
          ylabel(pLabel{iP},'Interpreter','latex','FontSize',labelFontSize); 
       end
       
       grid on
       xlim([5 40])
       
       if (iP ~= 4)
           h.XTickLabel = {};
       end
       ylim(ylimPrecision{iP,it});
    end   
    
end



%% Time consumption plot
H = figure(3);
clf
H.Position = [30,30,350,250];

plot(data.NSamplesTimingSimulation,1000*mean(data.t_nlls,2),'-','LineWidth',lw,'Color',[0 0 0])
hold on
plot(data.NSamplesTimingSimulation,1000*mean(data.t_lls,2),'--','LineWidth',lw,'Color',[0 0 0])
plot(data.NSamplesTimingSimulation,1000*mean(data.t_wtls,2),'-','LineWidth',lw,'Color',[1 1 1]/1.5)
ylabel('Time per fit [ms]','Interpreter','latex')
xlabel('Number of samples','Interpreter','latex');
l = legend('NLLS','LLS','WTLS');
l.Interpreter = 'Latex';
grid on;



%% Example curves
t = 0:3:300;
tref = 0:0.1:300;
H = figure(4);
clf
H.Position = [30,30,400,300];
Cp_ref = parkerAIF(tref/60,20/60);

% Get a parameter for the 2CXM
p = p_limits_2CXM(:,1) + 1/3*(p_limits_2CXM(:,2)-p_limits_2CXM(:,1));

% Get reference curve
Ct_ref = Model(tref/60,Cp_ref,p);

% SNR = 5
snr = 5;
noiseLevel = max(Ct_ref)/snr;
Ct_snr5 = interp1(tref,Ct_ref,t) + noiseLevel*randn(size(t));

% SNR = 40
snr = 40;
noiseLevel = max(Ct_ref)/snr;
Ct_snr40 = interp1(tref,Ct_ref,t) + noiseLevel*randn(size(t));

hold on
h = plot(t,Ct_snr5,'o');
h.MarkerFaceColor = [1 1 1];
h.LineWidth = 1.5; 
h.MarkerSize = 7;
h.Color = [1 1 1]/1.5;

h = plot(t,Ct_snr40,'s');
h.MarkerFaceColor = [1 1 1];
h.LineWidth = 1.5; 
h.MarkerSize = 7;
h.Color = [1 1 1]/5;

h = plot(tref,Ct_ref,'-k');
h.LineWidth = 2; 
ylim([-0.1 0.3]);
hold off
grid on
xlabel('Time [s]','Interpreter','latex')
ylabel('CA conc. [mM]','Interpreter','latex')
l = legend('SNR = 5','SNR = 40','Reference');
l.Interpreter = 'Latex';


disp('Script finished!')
disp('-------------------------------------------------------------------')
