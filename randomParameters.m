function p = randomParameters(p_limits)
 % Get random parameters from a specified interval.
 %
 % Input:
 % p_limits    - Min and max values of the parameters.
 %
 % Output:
 % p           - A random parameter sample, selected in the interval
 %               specified with a Gaussian distribution centered around the
 %               interval center with standard diviation s. Where s is 25 %
 %               of the parameter range.
 
 mu = mean(p_limits,2);
 sigma = (p_limits(:,2)-p_limits(:,1))/4;
 ok = false;
 while (~ok)
    p = sigma.*randn(4,1) + mu;
    if ~(any(p< p_limits(:,1)) || any(p > p_limits(:,2)))
        ok = true;
    end
 end
end