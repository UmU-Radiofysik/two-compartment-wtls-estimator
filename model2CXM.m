function Ct = model2CXM(t,Cp,p)
% The 2CX model 
%
% Input:
% t         - time vector [min]
% Cp        - AIF
% p         - The parameters [Fp, PS,vp, ve]
%
% Output:
% Ct        - Tissue CA conc. curve [mM]


    Fp = p(1);
    PS = p(2);
    vp = p(3);
    ve = p(4);
    
    
    Tp = vp/Fp;
    Te = ve/PS;
    T = Tp*(1+ve/vp);

    
    Tplus = 1/2*(T+Te+sqrt((T+Te)^2-4*Tp*Te));
    Tminus = 1/2*(T+Te-sqrt((T+Te)^2-4*Tp*Te));
    
    Ct = (Fp/(Tplus-Tminus)) * ((T-Tminus)*Tplus*expConv(Cp,t,Tplus) +  (Tplus-T)*Tminus*expConv(Cp,t,Tminus));
end