function p = get2CFMParam(x)
% Convert the result of the linear sulution to the 2CFM to the
% physiological parameters. 
%
% Input:
% x     - The result of the linear system. I.e. x = [alpha, beta ,gamma, Fp]
%         in reference: "D. Flouri, D. Lesnic, and S. P. Sourbron, 
%         �Fitting the two-compartment model in DCE-MRI by linear inversion,� 
%         Magn. Reson. Med., vol. 76, no. 3, pp. 998�1006, Sep. 2016."
%
% Output:
% p     - The physiological parameters. [Fp, PS, vp, ve]


a = x(1);
b = x(2);
g = x(3);
Fp = x(4);

T = g/(a*Fp);

if (b^2>4*a)
    Tp = (b-sqrt(b^2-4*a))/(2*a);
    Te = (b+sqrt(b^2-4*a))/(2*a);
else
    Te = b/(2*a);
    Tp = Te;
end

vp = Fp*Tp;
ve = Fp*(T-Tp);
PS = ve/Te;

p = [Fp,PS,vp,ve]';
end