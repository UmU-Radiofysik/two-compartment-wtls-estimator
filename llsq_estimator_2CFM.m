function [p,x] = llsq_estimator_2CFM(t,Cp,Ct,nonNeg)
% Linear least squares estimator to the 2CFM.
% Reference: D. Flouri, D. Lesnic, and S. P. Sourbron, �Fitting the two-compartment model in DCE-MRI by linear inversion,� 
%            Magn. Reson. Med., vol. 76, no. 3, pp. 998�1006, Sep. 2016.
%
% Input:
% t         - time vector [min]
% Cp        - AIF [mM]
% Ct        - Tissue CA curve [mM]
% nonNeg    - Optional if parameterers should be non-negative. Optional,
%             default = false.
%
% Output:
% p         - Estimated parameters [Fp, PS,vp, ve]
% x         - Estimated functions of p. I.e. x is the result of the solved
%             linear system of equations

    % Rearrange vectors to be consistant
    t = t(:);
    Cp = Cp(:);
    Ct = Ct(:);


    % Get primitive function
    CT = cumtrapz(t,Ct);
    CP = cumtrapz(t,Cp);
    CTT = cumtrapz(t,CT);
    CPP = cumtrapz(t,CP);

    % Solve the overdetermined system Ax = B
    A = [-CTT, -CT, CPP CP];
    
    if (nargin < 4)
       nonNeg = false; 
    end
    if (nonNeg)
        opts1=  optimset('display','off');
        x = lsqnonlin(A,Ct,opts1);
    else
        x = A\Ct;
    end


    % Calculate the value of the model parameters
    p = get2CFMParam(x);
end