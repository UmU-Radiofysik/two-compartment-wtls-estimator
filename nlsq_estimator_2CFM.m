% Non-linear least squares fit to the 2CFM
function p = nlsq_estimator_2CFM(t,Cp,Ct,settings)
% Perform a NLLS fit to the 2CFM.
% Input:
% t         - time vector [min]
% Cp        - AIF [mM]
% Ct        - Tissue CA curve [mM]
% settings  - Options for the lsqcurvefit algorithm
%
% Output:
% p         - Estimated parameters [Fp, PS,vp, ve]

% Inital guess
p0 = llsq_estimator_2CFM(t,Cp,Ct);
try
    p = lsqcurvefit(@(p,t)(model2CFM(t,Cp,p)),p0,t,Ct,[],[],settings);
catch e
    p = [NaN, NaN, NaN, NaN];
end
p = p(:);

end
