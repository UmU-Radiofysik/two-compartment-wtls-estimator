function Cp = parkerAIF(t,delay)
    % A population averaged AIF.
    %
    % Input:
    % t     - Time vector [min]
    % delay - Time delay [min]
    % 
    % Output:
    % Cp    - The AIF (plasma conc.)
    %
    % Reference: 
    % G. J. M. Parker, C. Roberts, A. Macdonald, G. A. Buonaccorsi, S. Cheung, 
    % D. L. Buckley, A. Jackson, Y. Watson, K. Davies, and G. C. Jayson, 
    % “Experimentally-derived functional form for a population-averaged 
    % high-temporal-resolution arterial input function for dynamic 
    % contrast-enhanced MRI.,” Magn. Reson. Med., vol. 56, no. 5, pp. 993–1000, 
    % Nov. 2006.
    
    alpha     = 1.05;
    beta      = 0.1685;
    s         = 38.078;
    tau       = 0.483;
    A         = [0.809 0.330];
    sigma     = [0.0563 0.132];
    T         = [0.1746 0.365];
    N         = 2;
    Hct       = 0.4;

    t = t-delay;
    Cp = alpha*exp(-beta*t)./(1+exp(-s*(t-tau)));
    for ii = 1:N
        Cp = Cp + A(ii)/(sigma(ii)*sqrt(2*pi))*exp(-(t-T(ii)).^2/(2*sigma(ii)^2));
    end
    Cp((t)<0) = 0;
    Cp = Cp ./ (1- Hct);
end