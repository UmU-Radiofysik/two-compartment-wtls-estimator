function [p,x,f] = newton_wtls_estimator_2CXM(t,Cp,Ct,nIter,relTolX,I,Q,Q2,dW,QQPerm)
    % WTLS estimator for the 2CXM
    %
    % Input:
    % t         - Time vector
    % Cp        - AIF
    % Ct        - Tissue CA conc. curve
    % nIter     - Max # of iterations
    % relTolX   - Termination criteria 
    % I         - Identity matrix n x n
    % Q         - Quadrature operator n x n
    % Q2        - Quadrature operator squared n x n
    % dW        - Derivative of the W matrix
    % QQPer     - Cell array of polynomials of the Q {Q2*Q2',Q2*Q',Q2 + Q2',Q*Q2',Q*Q',Q + Q'}
    %
    % Output:
    % p         - Estimated parameters [Fp, PS,vp, ve]
    % x         - Estimated functions of p. I.e. x is the result of the solved
    %             linear system of equations
    % f         - The objective function at each iteration.
    

    
    [x,f] = newton_wtls_estimator(t,Cp,Ct,nIter,relTolX,I,Q,Q2,dW,QQPerm);
    
    p = get2CXMParam(x); 
end