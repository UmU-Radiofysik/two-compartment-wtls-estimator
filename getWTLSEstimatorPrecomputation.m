function [I,Q,Q2,dW,QQperm] = getWTLSEstimatorPrecomputation(t)
% Create matrices used by the WTLS estimator.
%
% Input:
% t     - The sampling times used.
%
% Output: 
%
% I         - Identity matrix (n x n)
% Q         - Quadrature integral operator
% Q2        - Quadrature integral operator squared
% dW        - d/dx_i W(x)
% QQperm    - Precomputed matrixes baed on Q

nt = numel(t);
[ii,jj] = ndgrid(1:nt,1:nt);
Q = (t(2)-t(1))*(double((ii>=jj) -1/2*(ii==jj) - 1/2*(jj==1))) ;
Q(1,1) = (t(2)-t(1));


Q2 = Q*Q;
I = eye(size(Q));
dW = {[-Q2,zeros(size(Q))],[-Q,zeros(size(Q))],[zeros(size(Q)),Q2],[zeros(size(Q)),Q]};



QQperm = {Q2*Q2',Q2*Q',Q2 + Q2',Q*Q2',Q*Q',Q + Q'};